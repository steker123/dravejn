<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;

class PageController extends Controller {

    public function getIndex() {
        return view ('pages.index', ['pageName' => 'Index']);
    }

    public function getAbout() {
        return view('pages.about', ['pageName' => 'About']);
    }

    public function getContact() {
        return view ('pages.contact', ['pageName' => 'Contact']);
    }

    public function getPortfolio() {
        return view ('pages.portfolio', ['pageName' => 'Portfolio']);
    }

    public function getProducts() {
        return view ('pages.products' , ['pageName' => 'Products']);
    }

    public function getArticles($categoryId)
    {
        if (!empty($_GET["productId"])) {
            $productForId = Product::where([
                ['status', 1],
                ['id', $_GET["productId"]]
            ])->get();

            $productsForCategory  = Product::where([
                ['status', 1],
                ['category_id', $productForId[0]->category()->first()->getAttribute('id')]
            ])->orderBy('product_price')->get();

            $categoryName = $productForId[0]->category()->first()->getAttribute('category_name');

            $products = [
                'lowestPriceProduct' =>
                    [
                        'data' => $productForId[0],
                        'categoryName' => $categoryName,
                    ],
                'allProductsForCat' => $productsForCategory
            ];
        } else {
            $productsForCategory  = Product::where([
                ['status', 1],
                ['category_id', $categoryId]
            ])->orderBy('product_price')->get();

            $lowestPriceProduct = $productsForCategory[0];

            $categoryName = $lowestPriceProduct->category()->first()->getAttribute('category_name');

            $products = [
                'lowestPriceProduct' =>
                    [
                        'data' => $lowestPriceProduct,
                        'categoryName' => $categoryName,
                    ],
                'allProductsForCat' => $productsForCategory
            ];
        }

        return view('pages.articles')->with(compact('products'));
    }

    public function updateArticle(Request $request){

    }

}
