<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Section;
use App\Category;
use App\Product;
use Session;
use Image;

class ProductsController extends Controller
{
    public function products()
    {
        Session::put('page', 'products');
        $products = Product::with(['category' => function ($query) {
            $query->select('id', 'category_name');
        }, 'section' => function ($query) {
            $query->select('id', 'name');
        }])->get();
        return view('admin.products.products')->with(compact('products'));
    }

    public function updateProductStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data ['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            Product::where('id', $data['product_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'product_id' => $data['product_id']]);
        }
    }

    public function deleteProduct($id)
    {
        //Delete Product
        Product::where('id', $id)->delete();
        $message = 'Product has been deleted successfully!';
        Session::flash('success_message', $message);
        return redirect()->back();
    }

    public function addEditProduct(Request $request, $id = null)
    {
        if ($id == "") {
            $title = "Add Product";
            $product = new Product;
            $productdata = array();
            $message = "Product added succesfully!";
        } else {
            $title = "Edit Product";
            $productdata = Product::find($id);
            $productdata = json_decode(json_encode($productdata), true);
            $product = Product::find($id);
            $message = "Product updated succesfully!";

        }

        if ($request->isMethod('post')) {
            $data = $request->all();

            //Product Validation
            $rules = [
                'category_id' => 'required',
                'product_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'product_price' => 'required|numeric',
                'product_color' => 'required',
                'thickness_mm' => 'required',

            ];
            $customMessages = [
                'category_id.required' => 'Category is required',
                'product_name.regex' => 'Valid Product Name is required',
                'product_price.required' => 'Product Price is required',
                'product_price.numeric' => 'Valid Product Price is required',
                'product_color.required' => 'Product Color is required',
                'thickness_mm.required' => 'Thickness of a product is required',
            ];
            $this->validate($request, $rules, $customMessages);

            //Upload Product Image
            if ($request->hasFile('product_image')) {
                $image_tmp = $request->file('product_image');
                if ($image_tmp->isValid()) {
                    //Upload Images after Resize
                    $image_name = $image_tmp->getClientOriginalName();
                    $extension = $image_tmp->getClientOriginalExtension();
                    $imageName = $image_name . '-' . rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'img/admin_img/product_images/large/' . $imageName;
                    $medium_image_path = 'img/admin_img/product_images/medium/' . $imageName;
                    $small_image_path = 'img/admin_img/product_images/small/' . $imageName;
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(520, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(250, 300)->save($small_image_path);
                    $product->product_image = $large_image_path;

                }
            }

            //Save Product Details in products table

            $product->category_id = $data['category_id'];
            $product->product_name = $data['product_name'];
            $product->product_color = $data['product_color'];
            $product->product_price = $data['product_price'];
            $product->product_description = $data['product_description'];
            $product->thickness_mm = $data['thickness_mm'];
            $product->status = 1;
            $product->save();
            session::flash('success_message', $message);
            return redirect('admin/products');
        }

        //Filter Arrays - Lexan
        $thickness_mmArray = array('2', '2.5', '3', '4', '5', '6', '8', '10', '15', '16');
        $product_colorArray = array('Clear', 'Bronze', 'White Opal', 'Green', 'Blue');


        // Sections with Categories and Sub Categories
        $categories = Section::with('categories')->get();
        $categories = json_decode(json_encode($categories), true);
        /*echo "<pre>"; print_r($categories); die;*/


        return view('admin.products.add_edit_product')->with(compact('title', 'thickness_mmArray', 'product_colorArray', 'categories', 'productdata'));
    }

    public function deleteProductImage($id)
    {
        //Get Product Image
        $productImage = Product::select('product_image')->where('id', $id)->first();

        //Get Category Image Path
        $small_image_path = 'img/admin_img/product_images/small/';
        $medium_image_path = 'img/admin_img/product_images/medium/';
        $large_image_path = 'img/admin_img/product_images/large/';

        //Delete Product Image From product_images folder if exists (small)
        if (file_exists($small_image_path . $productImage->product_image)) {
            unlink($small_image_path . $productImage->product_image);
        }
        //Delete Product Image From product_images folder if exists (medium)
        if (file_exists($medium_image_path . $productImage->product_image)) {
            unlink($medium_image_path . $productImage->product_image);
        }
        //Delete Product Image From product_images folder if exists (large)
        if (file_exists($large_image_path . $productImage->product_image)) {
            unlink($large_image_path . $productImage->product_image);
        }

        //Delete Product Image from categories table
        Product::where('id', $id)->update(['product_image' => '']);

        $message = 'Product image has been deleted successfully!';
        session::flash('success_message', $message);
        return redirect()->back();
    }
}
