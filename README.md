Project setup:
1. Clone GitHub repo for this project locally
    - git clone https://nikola1575@bitbucket.org/steker123/dravejn.git
    
2. cd into your project

3. Install Composer Dependencies
    - composer install
    
4. Create a copy of your .env file
    1. cp .env.example .env
    2. setup DB_DATABASE name in .env (in my case "dravejn", but it can be called randomly)
    
5. Generate an app encryption key
    - php artisan key:generate
    
6. Create an empty database for application   
    * Make sure it is compatible with DB_DATABASE name in step 4.2
    
7. Migrate the database
    - php artisan migrate
    
8. Seed the database
    - php artisan db:seed

