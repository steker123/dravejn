$(document).ready(function () {

    //Check Admin password is correct or not
    $("#current_pwd").keyup(function () {
        var current_pwd = $("#current_pwd").val();
        $.ajax({
            type: 'post',
            url: '/admin/check-current-pwd',
            data: {current_pwd: current_pwd},
            success: function (resp) {
                if (resp == "false") {
                    $("#chkCurrentPwd").html("<label style='color: red' >Current Password is incorrect</label>");
                } else if (resp == "true") {
                    $("#chkCurrentPwd").html("<label style='color: green'>Current Password is correct</label>");
                }
            }, error: function () {
                alert("Error");
            }
        });
    });


    //Update Section Status
    $("#sections").on("click",".updateSectionStatus",function (){
        var status = $(this).text();
        var section_id = $(this).attr("section_id");

        // console.log("status", status);
        // console.log("section_id", section_id);

        $.ajax({
            type: 'post',
            url: '/admin/update-section-status',
            data: {status: status, section_id: section_id},
            success: function (resp) {
                console.log("resp", resp);
                console.log("selector", "#section-" + section_id);

                if (resp['status'] == 0) {
                    $("#section-" + section_id).text("Inactive");
                } else if (resp['status'] == 1) {
                    $("#section-" + section_id).text("Active");
                }
            }, error: function () {
                alert("Error");
            }
        });

    });
    //Update Category Status
    $(document).on("click",".updateCategoryStatus",function (){
        var status = $(this).text();
        var category_id = $(this).attr("category_id");
        $.ajax({
            type: 'post',
            url: '/admin/update-category-status',
            data: {
                status: status,
                category_id: category_id
            },
            success: function (resp) {
                if (resp['status'] == 0) {
                    $("#category-" + category_id).text("Inactive");
                } else if (resp['status'] == 1) {
                    $("#category-" + category_id).text("Active");
                }
            }, error: function () {
                alert("Error");
            }
        });
    });


    //Update Product Status
    $(document).on("click",".updateProductStatus",function (){
        var status = $(this).text();
        var product_id = $(this).attr("product_id");
        $.ajax({
            type: 'post',
            url: '/admin/update-product-status',
            data: {status: status, product_id: product_id},
            success: function (resp) {
                if (resp['status'] == 0) {
                        $("#product-" + product_id).text("Inactive");
                } else if (resp['status'] == 1) {
                    $("#product-" + product_id).text("Active");
                }
            }, error: function () {
                    alert("Error");
            }
        });
    });


    //Append Categories Level
    $('#section_id').change(function () {
        var section_id = $(this).val();
        $.ajax({
            type: 'post',
            url: '/admin/append-categories-level',
            data: {section_id: section_id},
            success: function (resp) {
                $("#appendCategoriesLevel").html(resp);
            }, error: function () {
                alert("Error");
            }
        });
    });

    //Confirm Deletion of Record
    /*$(".confirmDelete").click(function (){
       var name = $(this).attr("name");
       if (confirm("Are you sure to delete this "+name+"?")){
           return true;
       }
       return false;
    });*/


    //Confirm Deletion with SweetAlert
    $(document).on("click",".confirmDelete",function (){
        var record = $(this).attr("record");
        var recordid = $(this).attr("recordid")
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = "/admin/delete-" + record + "/" + recordid;
            }
        });
    });
});
