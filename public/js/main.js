jQuery(document).ready(function ($) {
// Porfolio - uses the magnific popup jQuery plugin
    $('.portfolio-popup').magnificPopup({
        type: 'image',
        removalDelay: 300,
        mainClass: 'mfp-fade',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function (openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        }
    });
});
// Intro carousel
var heroCarousel = $("#heroCarousel");
var heroCarouselIndicators = $("#hero-carousel-indicators");
heroCarousel.find(".carousel-inner").children(".carousel-item").each(function(index) {
    (index === 0) ?
        heroCarouselIndicators.append("<li data-target='#heroCarousel' data-slide-to='" + index + "' class='active'></li>"):
        heroCarouselIndicators.append("<li data-target='#heroCarousel' data-slide-to='" + index + "'></li>");
});

heroCarousel.on('slid.bs.carousel', function(e) {
    $(this).find('.carousel-content ').addClass('animate__animated animate__fadeInDown');
});
// Back to top button
const backToTopButton = document.querySelector("#back-to-top-btn");

window.addEventListener("scroll", scrollFunction);

function scrollFunction(){
    if (window.pageYOffset > 300){
        if (!backToTopButton.classList.contains("btnEntrance")){
            backToTopButton.classList.remove("btnExit");
            backToTopButton.classList.add("btnEntrance");
            backToTopButton.style.display = "block";
            }
        }
    else {
        if (backToTopButton.classList.contains("btnEntrance")){
            backToTopButton.classList.remove("btnEntrance");
            backToTopButton.classList.add("btnExit");
            setTimeout(function (){
                backToTopButton.style.display = "none";
            }, 250);
            }
        }
    }

backToTopButton.addEventListener("click", smoothScrollBackToTop);

function smoothScrollBackToTop() {
    const targetPosition = 0;
    const startPosition = window.pageYOffset;
    const distance = targetPosition - startPosition;
    const duration = 750;
    let start = null;

    window.requestAnimationFrame(step);

    function step(timestamp) {
        if (!start) start = timestamp;
        const progress = timestamp - start;
        window.scrollTo(0, easeInOutCubic(progress, startPosition, distance, duration));
        if (progress < duration) window.requestAnimationFrame(step);
    }
}

function easeInOutCubic(t, b, c, d) {
    t /= d/2;
    if (t < 1) return c/2*t*t*t + b;
    t -= 2;
    return c/2*(t*t*t + 2) + b;
};


//show only section with id ('sec' + sectionId)
function openSection(sectionId) {
    var secId = '#sec-' + sectionId;
    $('.sectionWrapper').css('display','none');
    $(secId).css('display','');
}


// Activate smooth scroll on page load with hash links in the url
$(document).ready(function() {
    if (window.location.hash) {
        var initial_nav = window.location.hash;
        if ($(initial_nav).length) {
            var scrollto = $(initial_nav).offset().top - scrolltoOffset;
            $('html, body').animate({
                scrollTop: scrollto
            }, 1000, 'easeInOutExpo');
        }
    }
});
// Init AOS
function aos_init() {
    AOS.init({
        duration: 1000,
        easing: "ease-in-out",
        once: true
    });
}
$(window).on('load', function() {
    aos_init();
});

/*--------------------------- test ------------------------------------ */
$(document).ready(function(){

    $('.target').click(function(){

        var productId = $(this).data('id');

        // AJAX request
        $.ajax({
            url: '/update-articles',
            type: 'post',
            data: {id: id},
            success: function(response){
                // Add response in Modal body
                $('.modal-body').html(response);
                $('#name').val(data["name"]);
                // Display Modal
                $('#empModal').modal('show');
            }
        });
    });
});

