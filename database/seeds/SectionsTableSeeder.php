<?php

use Illuminate\Database\Seeder;
use App\Section;
use Illuminate\Support\Facades\DB;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('sections')->delete();

        $sectionsRecords = [
            [
                'id' => 1,
                'name' => 'Lexan',
                'section_image' => 'img/admin_img/sections_images/lexan_section.jpg',
                'status' => 1
            ],
            [
                'id' => 2,
                'name' => 'Plexiglass',
                'section_image' => 'img/admin_img/sections_images/plexiglass_section.jpg',
                'status' => 1
            ],
            [
                'id' => 3,
                'name' => 'Additional material',
                'section_image' => 'img/admin_img/sections_images/additional_section.jpg',
                'status' => 1
            ],

        ];
        Section::insert($sectionsRecords);
    }
}
