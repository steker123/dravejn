<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Product;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();

        $productRecords = [
            [
                'category_id' => 1,
                'product_name' => 'Clear Lexan',
                'product_color' => 'Clear',
                'product_price' => '780',
                'product_image' => 'img/admin_img/product_images/large/lexan_clear.jpg-65957.jpg',
                'product_description' => 'clear lexan',
                'thickness_mm' => '4',
                'status' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];
        Product::insert($productRecords);
    }
}
