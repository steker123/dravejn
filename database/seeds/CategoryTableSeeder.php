<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryRecords = [
            ['id'=>'1','parent_id'=>0,'section_id'=>1,'category_name'=>'SUNLITE MULTIWALL POLYCARBONATE SHEET', 'category_image'=>'img/admin_img/category_images/67191.jpg' ,
               'description'=>'','url'=>'sunlite','meta_title'=>'','meta_description'=>'',
                'meta_keywords'=>'','status'=>1],
        ];
        Category::insert($categoryRecords);
    }
}
