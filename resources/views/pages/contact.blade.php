@extends('main')
@section('title' , '| Contact')
@section('content')
    <section id="phdr">
        <div class="row text-center">
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <h2>Contact Us</h2>
                <hr class="under">
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </section>
    <section id="chero">
        <div class="container gap100">
            <div class="row">
                <div class="col-md-6">
                    <h2>Reach Out To Us Today</h2>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dolore ipsam sed ut! Accusantium adipisci aspernatur commodi cum esse laborum laudantium molestias nobis nulla officiis porro quas reprehenderit, similique voluptates.</p>
                </div>
                <div class="col-md-6">

                </div>
            </div>
        </div>
    </section>

    <section id="cont">
        <div class="container gap100">
            <div class="row">
                <div class="col-md-12 text-center h2w">
                    <h2> How May We Help You?</h2>
                </div>
                <div class="col-md-4 text-light cad">
                    <div class="pft">
                        <p><i class="fas fa-phone-square"></i>063/459-301</p>
                    </div>
                    <div class="pft">
                        <p><i class="fas fa-fax"></i>017/411-965</p>
                    </div>
                    <div class="pft">
                        <p><i class="fas fa-envelope"></i>dravejn@live.com</p>
                    </div>
                    <div class="pft">
                        <p><i class="fas fa-map-marker-alt"></i> Sedam Sekretara Skoj-a 20 <br> Vranje 17500 <br> Republika Srbija </p>
                    </div>
                </div>
                <div class="col-md-8">
                    <div id="contact-form-section cform">
                        <div class="status alert alert-success" style="display:none"></div>
                            <form id="contact-form" class="contact pt-3" name="contact-form" method="post" action="send-mail.php">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control mail-field" required="required" placeholder="Your Email">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="subject" class="form-control" required="required" placeholder="Subject">
                                </div>
                                <div class="form-group">
                                    <textarea name="message" id="message" class="form-control" rows="8" required="required" placeholder="Send Message"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success rbtn">Submit</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d874.1424709116832!2d21.900296243395022!3d42.52867839701092!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13551fa7d0ec81ff%3A0xf5a846014a807117!2sSedam%20sekretara%20skoja%2020%2C%20Vranje!5e0!3m2!1sen!2srs!4v1596204226110!5m2!1sen!2srs"
                    width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </section>
@endsection
