@extends('main')
@section('title' , '| Home')
@section('content')

    <section id="hero">
        <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <!-- Slide 1 -->
                <div class="carousel-item active" style="background-image:url('img/front/banner1.jpg');">
                    <div class="carousel-container">
                        <div class="carousel-content animate__animated animate__fadeInUp">
                            <h2>Welcome to <span>Flattern</span></h2>
                            <p>Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil
                                ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse
                                doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                            <div class="text-center"><a href="" class="btn-get-started">Read More</a></div>
                        </div>
                    </div>
                </div>
                <!-- Slide 2 -->
                <div class="carousel-item" style="background-image:url('img/front/banner2.jpg');">
                    <div class="carousel-container">
                        <div class="carousel-content animate__animated animate__fadeInUp">
                            <h2>Lorem Ipsum Dolor</h2>
                            <p>Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil
                                ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse
                                doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                            <div class="text-center"><a href="" class="btn-get-started">Read More</a></div>
                        </div>
                    </div>
                </div>
                <!-- Slide 3 -->
                <div class="carousel-item" style="background-image: url('img/front/banner3.jpg');">
                    <div class="carousel-container">
                        <div class="carousel-content animate__animated animate__fadeInUp">
                            <h2>Sequi ea ut et est quaerat</h2>
                            <p>Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil
                                ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse
                                doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                            <div class="text-center"><a href="" class="btn-get-started">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon bx bx-left-arrow" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon bx bx-right-arrow" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

            <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        </div>

    </section><!-- End Hero -->

    <section id="portfolio">
        <div class="container marketing">

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7"  data-aos="fade-right">
                    <h2 class="featurette-heading">A World of Solutions. <span class="text-muted">It’ll blow your mind.</span>
                    </h2>
                    <p class="lead">Palram provides solutions to a wide array of applications at a range of market segments. Our global presence ensures the continuous availability of products with standardization for global customers.</p>
                </div>
                <div class="col-md-5"  data-aos="fade-left">
                    <div class="portfolio-item wow fadeInUp">
                        <img src="{{ url('img/front/index1.jpg') }}" alt="index1" width="100%" height="100%">
                    </div>
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7 order-md-2"  data-aos="fade-left">
                    <h2 class="featurette-heading">Oh yeah, it’s that good. <span
                            class="text-muted">See for yourself.</span></h2>
                    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                        euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus,
                        tellus ac cursus commodo.</p>
                </div>
                <div class="col-md-5 order-md-1"  data-aos="fade-right">
                    <div class="portfolio-item wow fadeInUp">
                        <img src="{{ url('img/front/index2.jpg') }}" alt="index2" width="100%" height="100%">
                    </div>
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7"  data-aos="fade-right">
                    <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
                    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                        euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus,
                        tellus ac cursus commodo.</p>
                </div>
                <div class="col-md-5"  data-aos="fade-left">
                    <div class="portfolio-item wow fadeInUp">
                        <img src="{{ url('img/front/index3.jpg') }}" alt="index3" width="100%" height="100%">
                    </div>
                </div>
            </div>

            <hr class="featurette-divider">

        </div>
    </section>

    <section id="cat">
        <div class="container">
            <div class="row">
                <div class="col-md-4 text-justify">
                    <div data-aos="fade-up" data-aos-delay="100">
                        <img src="{{ url ('img/front/cut1.jpg') }}" alt="1" width="100%" height="270px" class="mb-3">
                        <h3 class="text-center txt">Cut to size</h3>
                        <p>We offer to our customers cutting products manual or on cnc machines.</p>
                    </div>
                </div>
                <div class="col-md-4 text-justify">
                    <div data-aos="fade-up" data-aos-delay="200">
                        <img src="{{ url ('img/front/installation.jpg') }}" width="100%" height="270px" class="mb-3">
                        <h3 class="text-center txt">Installation</h3>
                        <p>We offer all our customers the installation of polycarbonate panels with a guarantee on the work performed.
                            We also cover / assemble with sheet metal and sandwich panels.</p>
                    </div>
                </div>
                <div class="col-md-4 text-justify">
                    <div data-aos="fade-up" data-aos-delay="300">
                        <img src="{{ url ('img/front/transport.jpg') }}" width="100%" height="270px" class="mb-3">
                        <h3 class="text-center txt">Transport</h3>
                        <p>We offer all our customers the possibility of delivering materials to our vehicles or express courier.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
