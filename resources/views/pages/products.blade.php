@extends('main')
@section('title' , '| Products')
@section('content')

@php
    use App\Section;
    $sections = Section::where('status', 1)->get();
@endphp


    <section id="phdr" >
        <div class="row text-center">
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <h2>Products</h2>
                <hr class="under">
                <div class="dash"></div>
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </section>
<section>
    <div class="container">
        <div class="row">
            @foreach($sections as $section)
                <div class="col-md-4 column" onclick="openSection({{$section->getAttribute('id')}});">
                    <h2 class="prdc_color">{{$section->getAttribute('name')}}</h2>
                    <img src="{{!empty($section->getAttribute('section_image')) ? $section->getAttribute('section_image') : "" }}" width="350px" height="270px" alt="">
                </div>
            @endforeach
        </div>
<hr class="productHR">
        @foreach($sections as $section)
            <div id="{{"sec-" . $section->getAttribute('id')}}" class="row sectionWrapper" style="display: none">
                    @foreach($section->categories()->where('status', 1)->get() as $category)
                        <div class="col-md-4 column">
                            <h4 class="prdc_color">{{$category->getAttribute('category_name')}}</h4>
                            <a href="{{url('articles', ['categoryId'=> $category->getAttribute('id')] )}}"><img src="{{!empty($category->getAttribute('category_image')) ? $category->getAttribute('category_image') : "" }}"
                                 width="350px" height="270px" alt=""></a>
                        </div>
                    @endforeach
            </div>
        @endforeach
    </div>
</section>
@endsection
