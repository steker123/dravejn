@extends('main')
@section('title' , '| Products - Lexan')
@section('content')

    @php
        $lowestPriceProduct = $products['lowestPriceProduct']['data'];
    @endphp

    <section id="phdr">
        <div class="row text-center">
            <div class="col-md-12">
                <h2>{{$products['lowestPriceProduct']['categoryName']}}</h2>
            </div>
        </div>
    </section>

    <section>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar -->
            <div class="bg-light border-right" id="sidebar-wrapper">
                <div class="sidebar-heading">Product List</div>
                <div class="list-group list-group-flush">
                    @foreach($products['allProductsForCat'] as $product)
                        <a href="/articles/{{$product->category()->first()->getAttribute('id')}}/?productId={{$product->getAttribute('id')}}" data-product-id="{{$product->getAttribute('id')}}"
                           class="list-group-item list-group-item-action bg-light target">{{$product->getAttribute('product_name') . " - " . $product->getAttribute('thickness_mm')}}</a>
                    @endforeach
                </div>
            </div>
            <!-- /#sidebar-wrapper -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="container gap100">
                        <div class="row">
                            <div class="col-md-5">
                                <img src="{{asset($lowestPriceProduct->getAttribute('product_image'))}}" width="100%"
                                     height="270px" class="mb-3" alt="">
                            </div>
                            <div class="col-md-6 offset-1">
                                <div class="form-group col-md-6 offset-3">
                                    <label for="exampleFormControlInput1">Product Name</label>
                                    <input type="read" class="form-control" id="product_name"
                                           value="{{$lowestPriceProduct->getAttribute('product_name') }}" readonly>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Color</label>
                                        <input type="read" class="form-control" id="color" name="color"
                                               value="{{$lowestPriceProduct->getAttribute('product_color') }}" readonly>

                                    </div>

                                    <div class="col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Thickness</label>
                                        <input type="read" class="form-control" id="thickness" name="thickness"
                                               value="{{$lowestPriceProduct->getAttribute('thickness_mm') }}" readonly>

                                    </div>

                                    <div class="form-group col-md-6 offset-3">
                                        <label for="productPrice">Your Price</label>
                                        <input type="read" class="form-control" id="productPrice"
                                               value="{{$lowestPriceProduct->getAttribute('product_price')}}" readonly>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-5 text-justify">
                                <h4>Product description</h4>
                                <p> {{$lowestPriceProduct->getAttribute('product_description')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
