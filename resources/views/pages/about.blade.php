@extends('main')
@section('title' , '| About')
@section('content')
    <section id="phdr">
        <div class="row text-center">
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <h2>About Us</h2>
                <hr class="under">
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </section>

    <div class="container">
        <div class="row gap100">
            <div class="col-md-6 text-justify" data-aos="fade-right">
                <h2>Our Mission</h2>
                &nbsp;
                <p>The company's headquarters are located in Vranje, within its own, functionally designed business complex in which there are business offices
                    (commercial, warehouse, design), fleet and warehouse space with a workshop. As one of the largest distributors of thermo-plastics in the country,
                    with many years of experience in distributing a wide range of products that find application in construction, advertising and industry, of various
                    shapes and qualities, we meet customer needs and meet the requirements of modern technology. Products from the MIS Thomas Plastic program are used
                    in several industries, and the program itself is constantly enriched with new products according to customer requirements.</p>
            </div>
            <div class="col-md-6"  data-aos="fade-left">
                <img src="{{ asset('img/front/roof.jpg') }}" class="img-fluid" alt="roof">
            </div>
        </div>
    </div>

    <section id="team">
        <div class="container gap100">
            <div class="row">
                <div class="col-md-4 text-center tm"  data-aos="fade-right">
                    <img src="img/front/t1.jpg" class="rounded-circle mb-4" alt="NP">
                    <h3>Nikola Petrovic</h3>
                    <h4>CEO / Owner</h4>
                    <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook-square"></i></a>
                    <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram-square"></i></a>
                    <a href="https://www.twitter.com" target="_blank"><i class="fab fa-twitter-square"></i></a>
                    <a href="https://www.linkedin.com" target="_blank"><i class="fab fa-linkedin"></i></a>
                </div>
                <div class="col-md-4 text-center tm"  data-aos="fade-up">
                    <img src="img/front/t2.jpg" class="rounded-circle mb-4" alt="MM">
                    <h3>Milica Milic</h3>
                    <h4>CEO / Sales Manager</h4>
                    <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook-square"></i></a>
                    <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram-square"></i></a>
                    <a href="https://www.twitter.com" target="_blank"><i class="fab fa-twitter-square"></i></a>
                    <a href="https://www.linkedin.com" target="_blank"><i class="fab fa-linkedin"></i></a>
                </div>
                <div class="col-md-4 text-center tm"  data-aos="fade-left">
                    <img src="img/front/t3.jpg" class="rounded-circle mb-4" alt="PP">
                    <h3>Petar Petrovic</h3>
                    <h4>Salesman</h4>
                    <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook-square"></i></a>
                    <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram-square"></i></a>
                    <a href="https://www.twitter.com" target="_blank"><i class="fab fa-twitter-square"></i></a>
                    <a href="https://www.linkedin.com" target="_blank"><i class="fab fa-linkedin"></i></a>
                </div>
            </div>
        </div>
    </section>
@endsection
