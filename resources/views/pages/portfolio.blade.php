@extends('main')
@section('title' , '| Portfolio')
@section('content')

    <section id="phdr">
        <div class="row text-center">
            <div class="col-md-12">
                <h2>Our Portfolio</h2>
                <hr class="under">
            </div>
        </div>
    </section>

    <section id="portfolio" class="wow fadeInUp">
        <div class="container">
            <div class="section-header port" data-aos="fade-down" >
                <h2 class="text-center">Gallery</h2>
                <p> </p>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row no-gutters"data-aos="fade-up">
                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/1.jpg') }}" class="portfolio-popup">
                            <img src="{{ url('img/portfolioIMG/1.jpg') }}" alt="1">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/2.jpg') }}" class="portfolio-popup">
                            <img src="{{ url('img/portfolioIMG/2.jpg') }}" alt="2">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/3.jpg') }}" class="portfolio-popup">
                            <img src="{{ url('img/portfolioIMG/3.jpg') }}" alt="3">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/4.jpg') }}" class="portfolio-popup">
                            <img src="{{ url ('img/portfolioIMG/4.jpg') }}" alt="4">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/5.jpg') }}" class="portfolio-popup">
                            <img src="{{ url ('img/portfolioIMG/5.jpg') }}" alt="5">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/6.jpg') }}" class="portfolio-popup">
                            <img src="{{ url ('img/portfolioIMG/6.jpg') }}" alt="6">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/7.jpg') }}" class="portfolio-popup">
                            <img src="{{ url ('img/portfolioIMG/7.jpg') }}" alt="7">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/8.jpg') }}" class="portfolio-popup">
                            <img src="{{ url ('img/portfolioIMG/8.jpg') }}" alt="8">
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/9.jpg') }}" class="portfolio-popup">
                            <img src="{{ url ('img/portfolioIMG/9.jpg') }}" alt="9">
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/10.jpg') }}" class="portfolio-popup">
                            <img src="{{ url ('img/portfolioIMG/10.jpg') }}" alt="10">
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/11.jpg') }}" class="portfolio-popup">
                            <img src="{{ url ('img/portfolioIMG/11.jpg') }}" alt="11">
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="portfolio-item wow fadeInUp">
                        <a href="{{ url ('img/portfolioIMG/12.jpg') }}" class="portfolio-popup">
                            <img src="{{ url ('img/portfolioIMG/12.jpg') }}" alt="12">
                        </a>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection
