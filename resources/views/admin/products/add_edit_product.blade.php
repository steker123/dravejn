@extends('admin.admin_layouts.admin_layout')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>


        <section class="content">
            <div class="container-fluid">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 10px">
                        {{ Session::get('success_message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form name="productForm" id="productForm"
                      @if(empty($productdata['id'])) action="{{ url('admin/add-edit-product') }}"
                      @else action="{{ url('admin/add-edit-product/'.$productdata['id']) }}" @endif
                      method="post" enctype="multipart/form-data">@csrf
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Category</label>
                                        <select name="category_id" id="category_id" class="form-control select2"
                                                style="width: 100%;">
                                            <option value="">Select</option>
                                            @foreach($categories as $section)
                                                <optgroup label="{{ $section['name'] }}"></optgroup>
                                                @foreach($section['categories'] as $category)
                                                    <option value="{{ $category['id'] }}"
                                                            @if(!empty(@old('category_id')) && $category['id'] == @old('category_id')) selected=""
                                                            @elseif(!empty($productdata['category_id']) && $productdata['category_id']==$category['id'])  selected="" @endif>
                                                        &nbsp;&nbsp;--&nbsp;&nbsp;{{ $category['category_name'] }}</option>
                                                    @foreach($category['subcategories'] as $subcategory)
                                                        <option value="{{ $subcategory['id'] }}"
                                                                @if(!empty(@old('category_id')) && $subcategory['id'] == @old('category_id')) selected=""
                                                                @elseif(!empty($productdata['category_id']) && $productdata['category_id']==$subcategory['id'])  selected="" @endif>
                                                            &nbsp;&nbsp;&nbsp;&nbsp; --
                                                            &nbsp;&nbsp;{{ $subcategory['category_name'] }}</option>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="product_description">Product Description</label>
                                        <textarea name="product_description" id="product_description"
                                                  class="form-control" rows="3"
                                                  placeholder="Enter ...">@if(!empty($productdata['product_description'])) {{ $productdata['product_description'] }} @else {{ old('product_description') }} @endif</textarea>
                                    </div>&nbsp;
                                    <div class="form-group">
                                        <label for="example">Product Image</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="product_image"
                                                       name="product_image">
                                                <label class="custom-file-label" for="product_image">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Upload</span>
                                            </div>
                                        </div>
                                        @if(!empty($productdata['product_image']))
                                            <div><img style="width:80px; margin-top: 5px;"
                                                      src="{{asset( $productdata['product_image']) }}">
                                                &nbsp;
                                                <a class="confirmDelete" href="javascript:void(0)"
                                                   record="product-image"
                                                   recordid=" {{ $productdata['id'] }}" <?php /*href="{{ url('admin/delete-product-image/'.$productdata['id'])  }}" */ ?>>
                                                    Delete Image</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="product_name">Product Name</label>
                                        <input type="text" class="form-control" name="product_name" id="product_name"
                                               placeholder="Enter product Name"
                                               @if(!empty($productdata['product_name'])) value="{{ $productdata['product_name'] }}"
                                               @else value="{{ old('product_name') }}" @endif >
                                    </div>
                                    <div class="form-group">
                                        <label for="product_price">Product Price</label>
                                        <input type="text" class="form-control" name="product_price" id="product_price"
                                               placeholder="Enter price"
                                               @if(!empty($productdata['product_price'])) value="{{ $productdata['product_price'] }}"
                                               @else value="{{ old('product_price') }}" @endif >
                                    </div>
                                    <div class="form-group">
                                        <label>Select Product Color</label>
                                        <select name="product_color" id="product_color" class="form-control select2"
                                                style="width: 100%;">
                                            <option value="">Select</option>
                                            @foreach($product_colorArray as $product_color)
                                                <option value="{{ $product_color }}"
                                                        @if(!empty($productdata['product_color']) && $productdata['product_color']==$product_color) selected="" @endif>{{ $product_color }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Select Product Thickness</label>
                                        <select name="thickness_mm" id="thickness_mm" class="form-control select2"
                                                style="width: 100%;">
                                            <option value="">Select</option>
                                            @foreach($thickness_mmArray as $thickness_mm)
                                                <option value="{{ $thickness_mm }}"
                                                        @if(!empty($productdata['thickness_mm']) && $productdata['thickness_mm']==$thickness_mm) selected="" @endif>{{ $thickness_mm }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{--                                    <div class="form-group">--}}
                                    {{--                                        <label for="product_color">Product Color</label>--}}
                                    {{--                                        <input type="text" class="form-control" name="product_color" id="product_color"--}}
                                    {{--                                               placeholder="Enter Product Color"--}}
                                    {{--                                               @if(!empty($productdata['product_color'])) value="{{ $productdata['product_color'] }}"--}}
                                    {{--                                               @else value="{{ old('product_color') }}" @endif>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <div class="form-group">--}}
                                    {{--                                        <label for="thickness_mm">Thickness</label>--}}
                                    {{--                                        <input type="text" class="form-control" name="thickness_mm" id="thickness_mm"--}}
                                    {{--                                               placeholder="Enter Product Thickness"--}}
                                    {{--                                               @if(!empty($productdata['thickness_mm"'])) value="{{ $productdata['thickness_mm"'] }}"--}}
                                    {{--                                               @else value="{{ old('thickness_mm"') }}" @endif>--}}
                                    {{--                                    </div>--}}
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>




@endsection
