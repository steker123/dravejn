<!doctype html>
<html lang="en">

@include('partials._header')

    <body>

        @include('partials._nav')


        @yield('content')


        @include('partials._pfooter')




    <button id="back-to-top-btn"><i class="fas fa-angle-double-up"></i></button>
        @include('partials._javascript')


    </body>
</html>
