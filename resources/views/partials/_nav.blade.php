<header id="header">
    <nav class="navbar navbar-expand-lg navbar-light bg-light mynav">
        <a class="navbar-brand" href="/"><img src="{{ asset('img/front/logo.png') }}" width="100px" height="60px"
                                              alt="Dravejn enterijeri logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item {{!empty($pageName) && $pageName == 'Index' ? 'active' : '' }}">
                    <a class="nav-link" href="/">HOME <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item {{!empty($pageName) && $pageName == 'About' ? 'active' : '' }}">
                    <a class="nav-link" href="/about">ABOUT</a>
                </li>
                <li class="nav-item {{!empty($pageName) && $pageName == 'Portfolio' ? 'active' : '' }}">
                    <a class="nav-link" href="/portfolio">PORTFOLIO</a>
                </li>
                <li class="nav-item {{!empty($pageName) && $pageName == 'Products' ? 'active' : '' }}">
                    <a class="nav-link" href="/products">PRODUCTS</a>
                </li>

                <li class="nav-item {{!empty($pageName) && $pageName == 'Contact' ? 'active' : '' }}">
                    <a class="nav-link" href="/contact">CONTACT</a>
                </li>

            </ul>
            <div class="ml-auto smed">
                <a href="https://www.facebook.com/nikola.petrovic.737/" target="_blank"><i class="fab fa-facebook-square"></i></a>
                <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram-square"></i></a>
                <a href="https://www.youtube.com" target="_blank"><i class="fab fa-youtube-square"></i></a>
            </div>
        </div>
    </nav>
</header>
