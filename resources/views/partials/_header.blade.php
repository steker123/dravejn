
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ url ('img/front/favicon.png')}}">
            <!-- Bootstrap CSS -->
    <link rel="stylesheet"  type="text/css" href="{{ url('css/bootstrap.min.css') }}" >
    <link rel="stylesheet" href="{{ url('css/vendor/boxicons/css/boxicons.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/vendor/animate.css/animate.min.css') }}">
    <link rel="stylesheet" href="{{url('css/vendor/aos/aos.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('lib/magnific-popup/magnific-popup.css')}}" >



    <title>Dravejn @yield('title')</title> <!-- Change title -->
</head>

