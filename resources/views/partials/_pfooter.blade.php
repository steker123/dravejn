<section id="pfooter">
    <div class="container gap100v1">
        <div class="row text-light">
            <div class="col-md-4 p-3 pf">
                <h3 class="text-center h3w">Quick Links</h3>
                <div class="pft">
                    <a href="/"><p>> Home</p></a>
                </div>
                <div class="pft">
                    <a href="about"><p>> About Us</p></a>
                </div>
                <div class="pft">
                    <a href="portfolio"><p>> Portfolio</p></a>
                </div>
                <div class="pft">
                    <a href="products"><p>> Products</p></a>
                </div>
                <div class="pft">
                    <a href="index"><p>> Contact</p></a>
                </div>
            </div>
            <div class="col-md-4 p-3 pf">
                <h3 class="text-center h3w">Location</h3>
                    <div class="p-4 mt-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d874.1424709116832!2d21.900296243395022!3d42.52867839701092!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13551fa7d0ec81ff%3A0xf5a846014a807117!2sSedam%20sekretara%20skoja%2020%2C%20Vranje!5e0!3m2!1sen!2srs!4v1596204226110!5m2!1sen!2srs"
                                width="100%" height="270" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                    <div>

                        <p class="text-light copy text-center">Copyright © 2020 Dravejn enterijeri</p>

                    </div>
            </div>
            <div class="col-md-4 p-3 pf">
                <h3 class="text-center h3w">Reach out</h3>
                    <div class="pft">
                        <p><i class="fas fa-phone-square"></i>063/459-301</p>
                    </div>
                    <div class="pft">
                        <p><i class="fas fa-fax"></i>017/411-965</p>
                    </div>
                    <div class="pft">
                        <p><i class="fas fa-envelope"></i>dravejn@live.com</p>
                    </div>
                    <div class="pft">
                        <p><i class="fas fa-map-marker-alt"></i> Sedam Sekretara Skoj-a 20 <br> Vranje 17500 <br> Republika Srbija <a class="btn btn-contact" href="contact">Contact</a> </p>
                    </div>
            </div>
        </div>
    </div>
</section>
